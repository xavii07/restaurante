$("#form-add-provincia").validate({
  rules: {
    bandera_xg: {
      extesion: "png|jpg|jpeg",
    },
  },
  messages: {
    nombre_xg: {
      required: "Por favor ingrese el nombre de la provincia",
    },
    capital_xg: {
      required: "Por favor ingrese la capital de la provincia",
    },
    alcalde_xg: {
      required: "Por favor ingrese el alcalde de la provincia",
    },
    region_xg: {
      required: "Por favor ingrese la region de la provincia",
    },
    bandera_xg: {
      required: "Por favor ingrese la bandera de la provincia",
    },
    sxgc_fecha_fundacion: {
      required: "Por favor ingrese la fecha de fundacion de la provincia",
    },
    sxgc_carta_fundacion: {
      required: "Por favor ingrese la carta de fundacion de la provincia",
    },
  },
});

$("#form-add-cliente").validate({
  rules: {
    cedula_xg: {
      required: true,
      digits: true,
      minlength: 10,
      maxlength: 10,
    },
    nombres_xg: {
      required: true,
      minlength: 5,
      maxlength: 30,
    },
    apellidos_xg: {
      required: true,
      minlength: 5,
      maxlength: 30,
    },
    direccion_xg: {
      required: true,
      minlength: 5,
      maxlength: 100,
    },
    telefono_xg: {
      required: true,
      digits: true,
      minlength: 10,
      maxlength: 10,
    },
    provincia_xg: {
      required: true,
    },
  },
  messages: {
    cedula_xg: {
      required: "Por favor ingrese la cedula del cliente",
      digits: "Por favor ingrese solo numeros",
      minlength: "Por favor ingrese 10 digitos",
      maxlength: "Por favor ingrese 10 digitos",
    },
    nombres_xg: {
      required: "Por favor ingrese el nombre del cliente",
      minlength: "El nombre debe tener al menos 5 caracteres",
      maxlength: "El nombre debe tener maximo 30 caracteres",
    },
    apellidos_xg: {
      required: "Por favor ingrese el apellido del cliente",
      minlength: "El apellido debe tener al menos 5 caracteres",
      maxlength: "El apellido debe tener maximo 30 caracteres",
    },
    direccion_xg: {
      required: "Por favor ingrese la direccion del cliente",
      minlength: "La direccion debe tener al menos 5 caracteres",
      maxlength: "La direccion debe tener maximo 100 caracteres",
    },
    telefono_xg: {
      required: "Por favor ingrese el telefono del cliente",
      digits: "Por favor ingrese solo numeros",
      minlength: "Por favor ingrese 10 digitos",
      maxlength: "Por favor ingrese 10 digitos",
    },
    provincia_xg: {
      required: "Por favor seleccione una provincia",
    },
  },
});

$("#form-add-tipo").validate({
  rules: {
    nombre_xg: {
      required: true,
      maxlength: 50,
    },
    descripcion_xg: {
      required: true,
      maxlength: 250,
    },
    prioridad_xg: {
      required: true,
      max: 10,
    },
  },
  messages: {
    nombre_xg: {
      required: "Por favor ingrese el nombre del tipo",
      maxlength: "El nombre debe tener maximo 50 caracteres",
    },
    descripcion_xg: {
      required: "Por favor ingrese la descripcion del tipo",
      maxlength: "La descripcion debe tener maximo 250 caracteres",
    },
    prioridad_xg: {
      required: "Por favor ingrese la prioridad del tipo",
      max: "La prioridad debe ser maximo 10",
    },
    color: {
      required: "Por favor seleccione un color",
    },
    imagen: {
      required: "Por favor seleccione una imagen",
    },
  },
});

$("#form-add-pedido").validate({
  rules: {
    fecha_xg: {
      required: true,
    },
    estado_xg: {
      required: true,
    },
    cliente_xg: {
      required: true,
    },
    observaciones_xg: {
      required: true,
      maxlength: 250,
    },
  },
  messages: {
    fecha_xg: {
      required: "Por favor ingrese la fecha del pedido",
    },
    estado_xg: {
      required: "Por favor seleccione un estado",
    },
    cliente_xg: {
      required: "Por favor seleccione un cliente",
    },
    observaciones_xg: {
      required: "Por favor ingrese las observaciones del pedido",
      maxlength: "Las observaciones deben tener maximo 250 caracteres",
    },
    total_xg: {
      required: "Por favor ingrese el total del pedido",
    },
  },
});

$("#form-add-platillo").validate({
  rules: {
    nombre_xg: {
      required: true,
      maxlength: 50,
    },
    descripcion_xg: {
      required: true,
      maxlength: 250,
    },
    precio_xg: {
      required: true,
      number: true,
    },
    tipo_xg: {
      required: true,
    },
    imagen_xg: {
      required: true,
    },
  },
  messages: {
    nombre_xg: {
      required: "Por favor ingrese el nombre del platillo",
      maxlength: "El nombre debe tener maximo 50 caracteres",
    },
    descripcion_xg: {
      required: "Por favor ingrese la descripcion del platillo",
      maxlength: "La descripcion debe tener maximo 250 caracteres",
    },
    precio_xg: {
      required: "Por favor ingrese el precio del platillo",
      number: "Por favor ingrese solo numeros",
    },
    tipo_xg: {
      required: "Por favor seleccione un tipo",
    },
    imagen_xg: {
      required: "Por favor seleccione una imagen",
    },
  },
});

$("#form-add-detalle").validate({
  rules: {
    cantidad_xg: {
      required: true,
      number: true,
    },
    comentarios_xg: {
      required: true,
      maxlength: 250,
    },
    platillo_xg: {
      required: true,
    },
    pedido_xg: {
      required: true,
    },
  },
  messages: {
    cantidad_xg: {
      required: "Por favor ingrese la cantidad del detalle",
      number: "Por favor ingrese solo numeros",
    },
    subtotal_xg: {
      required: "Por favor ingrese el subtotal del detalle",
    },
    comentarios_xg: {
      required: "Por favor ingrese los comentarios del detalle",
      maxlength: "Los comentarios deben tener maximo 250 caracteres",
    },
    platillo_xg: {
      required: "Por favor seleccione un platillo",
    },
    pedido_xg: {
      required: "Por favor seleccione un pedido",
    },
  },
});

$("#form-add-ingrediente").validate({
  rules: {
    nombre_xg: {
      required: true,
      maxlength: 50,
    },
    descripcion_xg: {
      required: true,
      maxlength: 250,
    },
    cantidad_xg: {
      required: true,
      number: true,
    },
    proveedor_xg: {
      required: true,
    },
    precio_xg: {
      required: true,
      number: true,
    },
    imagen_xg: {
      required: true,
    },
  },
  messages: {
    nombre_xg: {
      required: "Por favor ingrese el nombre del ingrediente",
      maxlength: "El nombre debe tener maximo 50 caracteres",
    },
    descripcion_xg: {
      required: "Por favor ingrese la descripcion del ingrediente",
      maxlength: "La descripcion debe tener maximo 250 caracteres",
    },
    cantidad_xg: {
      required: "Por favor ingrese la cantidad del ingrediente",
      number: "Por favor ingrese solo numeros",
    },
    proveedor_xg: {
      required: "Por favor seleccione un proveedor",
    },
    precio_xg: {
      required: "Por favor ingrese el precio del ingrediente",
      number: "Por favor ingrese solo numeros",
    },
    imagen_xg: {
      required: "Por favor seleccione una imagen",
    },
    unidad_xg: {
      required: "Por favor seleccione una unidad",
    },
  },
});

$("#form-add-receta").validate({
  rules: {
    cantidad_xg: {
      required: true,
      number: true,
    },
    observaciones_xg: {
      required: true,
      maxlength: 250,
    },
    instrucciones_xg: {
      required: true,
      maxlength: 250,
    },
    ingrediente_xg: {
      required: true,
    },
    platillo_xg: {
      required: true,
    },
  },
  messages: {
    cantidad_xg: {
      required: "Por favor ingrese la cantidad de la receta",
      number: "Por favor ingrese solo numeros",
    },
    observaciones_xg: {
      required: "Por favor ingrese las observaciones de la receta",
      maxlength: "Las observaciones deben tener maximo 250 caracteres",
    },
    instrucciones_xg: {
      required: "Por favor ingrese las instrucciones de la receta",
      maxlength: "Las instrucciones deben tener maximo 250 caracteres",
    },
    ingrediente_xg: {
      required: "Por favor seleccione un ingrediente",
    },
    platillo_xg: {
      required: "Por favor seleccione un platillo",
    },
  },
});

$("#form-send-email").validate({
  rules: {
    destinatario: {
      required: true,
      email: true,
    },
    asunto: {
      required: true,
      maxlength: 50,
    },
    cuerpo: {
      required: true,
    },
  },
  messages: {
    destinatario: {
      required: "Por favor ingrese el destinatario",
      email: "Por favor ingrese un correo valido",
    },
    asunto: {
      required: "Por favor ingrese el asunto",
      maxlength: "El asunto debe tener maximo 50 caracteres",
    },
    cuerpo: {
      required: "Por favor ingrese el cuerpo",
    },
  },
});
