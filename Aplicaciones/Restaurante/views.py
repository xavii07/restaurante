from django.shortcuts import render, redirect
from .models import Provincia, Cliente, Pedido, Tipo, Platillo, Ingrediente, Receta, Detalle
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings

# Create your views here.
def home(request):
    return render(request, 'home.html')

#TODO: Provincias
def listadoProvincias(request):
    provinciasDbb = Provincia.objects.all()
    return render(request, 'provincias.html', {'provincias': list(enumerate(provinciasDbb, start=1))})

def crearProvincia(request):
    nombre = request.POST['nombre_xg']
    capital = request.POST['capital_xg']
    region = request.POST['region_xg']
    alcalde = request.POST['alcalde_xg']
    bandera = request.FILES.get('bandera_xg')
    fecha_fundacion = request.POST['sxgc_fecha_fundacion']
    carta_fundacion = request.FILES.get('sxgc_carta_fundacion')
    provinciaDb = Provincia.objects.create(nombre_xg=nombre, capital_xg=capital, region_xg=region, alcalde_xg=alcalde, bandera_xg=bandera,sxgc_fecha_fundacion=fecha_fundacion, sxgc_carta_fundacion=carta_fundacion)

    messages.success(request, 'Provincia creada correctamente')
    return redirect('/provincias')

def eliminarProvincia(request, id):
    provinciaDb = Provincia.objects.get(id_xg=id)
    provinciaDb.delete()

    messages.success(request, 'Provincia eliminada correctamente')
    return redirect('/provincias')

def editarProvinciaView(request, id):
    provinciaDb = Provincia.objects.get(id_xg=id)
    return render(request, 'editarProvincia.html', {'provincia': provinciaDb})

def actualizarProvincia(request):
    id = request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    capital = request.POST['capital_xg']
    region = request.POST['region_xg']
    alcalde = request.POST['alcalde_xg']
    bandera = request.FILES.get('bandera_xg') # Si no se envía nada, retorna None
    fecha_fundacion = request.POST['sxgc_fecha_fundacion']
    carta_fundacion = request.FILES.get('sxgc_carta_fundacion')

    provinciaEditar = Provincia.objects.get(id_xg=id)

    provinciaEditar.nombre_xg = nombre
    provinciaEditar.capital_xg = capital
    provinciaEditar.region_xg = region
    provinciaEditar.alcalde_xg = alcalde
    provinciaEditar.sxgc_fecha_fundacion = fecha_fundacion
    if carta_fundacion is not None:
        provinciaEditar.sxgc_carta_fundacion = carta_fundacion
    if bandera is not None:
        provinciaEditar.bandera_xg = bandera
    provinciaEditar.save()

    messages.success(request, 'Provincia actualizada correctamente')
    return redirect('/provincias')

#TODO: Clientes
def listadoClientes(request):
    clientesDbb = Cliente.objects.all()
    provinciasDbb = Provincia.objects.all()
    return render(request, 'clientes.html', {'clientes': list(enumerate(clientesDbb, start=1)), 'provincias': provinciasDbb})

def crearCliente(request):
    cedula = request.POST['cedula_xg']
    nombres = request.POST['nombres_xg']
    apellidos = request.POST['apellidos_xg']
    direccion = request.POST['direccion_xg']
    telefono = request.POST['telefono_xg']
    provincia = request.POST['provincia_xg']
    provinciaSeleccionada = Provincia.objects.get(id_xg=provincia)
    newCliente = Cliente.objects.create(cedula_xg=cedula, nombres_xg=nombres, apellidos_xg=apellidos, direccion_xg=direccion, telefono_xg=telefono, provincia_xg=provinciaSeleccionada)

    messages.success(request, 'Cliente creado correctamente')
    return redirect('/clientes')

def eliminarCliente(request, id):
    clienteDb = Cliente.objects.get(id_xg=id)
    clienteDb.delete()

    messages.success(request, 'Cliente eliminado correctamente')
    return redirect('/clientes')

def editarClienteView(request, id):
    clienteDb = Cliente.objects.get(id_xg=id)
    provinciasDbb = Provincia.objects.all()
    return render(request, 'editarCliente.html', {'cliente': clienteDb, 'provincias': provinciasDbb})

def actualizarCliente(request):
    id = request.POST['id_xg']
    cedula = request.POST['cedula_xg']
    nombres = request.POST['nombres_xg']
    apellidos = request.POST['apellidos_xg']
    direccion = request.POST['direccion_xg']
    telefono = request.POST['telefono_xg']
    provincia = request.POST['provincia_xg']
    provinciaSeleccionada = Provincia.objects.get(id_xg=provincia)

    clienteEditar = Cliente.objects.get(id_xg=id)
    clienteEditar.cedula_xg = cedula
    clienteEditar.nombres_xg = nombres
    clienteEditar.apellidos_xg = apellidos
    clienteEditar.direccion_xg = direccion
    clienteEditar.telefono_xg = telefono
    clienteEditar.provincia_xg = provinciaSeleccionada
    clienteEditar.save()

    messages.success(request, 'Cliente actualizado correctamente')
    return redirect('/clientes')

#TODO: Pedidos
def listadoPedidos(request):
    pedidosDbb = Pedido.objects.all()
    clientesDbb = Cliente.objects.all()
    return render(request, 'pedidos.html', {'pedidos': list(enumerate(pedidosDbb, start=1)), 'clientes': clientesDbb})

def crearPedido(request):
    fecha = request.POST['fecha_xg']
    estado = request.POST['estado_xg']
    total = request.POST['total_xg']
    observaciones = request.POST['observaciones_xg']
    cliente = request.POST['cliente_xg']
    clienteSeleccionado = Cliente.objects.get(id_xg=cliente)

    newPedido = Pedido.objects.create(fecha_xg=fecha, estado_xg=estado, total_xg=total, observaciones_xg=observaciones, cliente_xg=clienteSeleccionado)

    messages.success(request, 'Pedido creado correctamente')
    return redirect('/pedidos')

def eliminarPedido(request, id):
    pedidoDb = Pedido.objects.get(id_xg=id)
    pedidoDb.delete()

    messages.success(request, 'Pedido eliminado correctamente')
    return redirect('/pedidos')

def editarPedidoView(request, id):
    pedidoDb = Pedido.objects.get(id_xg=id)
    clientesDbb = Cliente.objects.all()
    return render(request, 'editarPedido.html', {'pedido': pedidoDb, 'clientes': clientesDbb})

def actualizarPedido(request):
    id = request.POST['id_xg']
    fecha = request.POST['fecha_xg']
    estado = request.POST['estado_xg']
    total = request.POST['total_xg']
    observaciones = request.POST['observaciones_xg']
    cliente = request.POST['cliente_xg']
    clienteSeleccionado = Cliente.objects.get(id_xg=cliente)

    pedidoEditar = Pedido.objects.get(id_xg=id)
    pedidoEditar.fecha_xg = fecha
    pedidoEditar.estado_xg = estado
    pedidoEditar.total_xg = total
    pedidoEditar.observaciones_xg = observaciones
    pedidoEditar.cliente_xg = clienteSeleccionado
    pedidoEditar.save()

    messages.success(request, 'Pedido actualizado correctamente')
    return redirect('/pedidos')

#TODO: Tipos
def listadoTipos(request):
    tiposDbb = Tipo.objects.all()
    return render(request, 'tipos.html', {'tipos': list(enumerate(tiposDbb, start=1))})

def crearTipo(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    prioridad = request.POST['prioridad_xg']
    color = request.POST['color_xg']
    imagen = request.FILES.get('imagen_xg')

    newTipo = Tipo.objects.create(nombre_xg=nombre, descripcion_xg=descripcion, prioridad_xg=prioridad, color_xg=color, imagen_xg=imagen)

    messages.success(request, 'Tipo creado correctamente')
    return redirect('/tipos')

def eliminarTipo(request, id):
    tipoDb = Tipo.objects.get(id_xg=id)
    tipoDb.delete()

    messages.success(request, 'Tipo eliminado correctamente')
    return redirect('/tipos')

def editarTipoView(request, id):
    tipoDb = Tipo.objects.get(id_xg=id)
    return render(request, 'editarTipo.html', {'tipo': tipoDb})

def actualizarTipo(request):
    id = request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    prioridad = request.POST['prioridad_xg']
    color = request.POST['color_xg']
    imagen = request.FILES.get('imagen_xg')

    tipoEditar = Tipo.objects.get(id_xg=id)
    tipoEditar.nombre_xg = nombre
    tipoEditar.descripcion_xg = descripcion
    tipoEditar.prioridad_xg = prioridad
    tipoEditar.color_xg = color
    if imagen is not None:
        tipoEditar.imagen_xg = imagen
    tipoEditar.save()

    messages.success(request, 'Tipo actualizado correctamente')
    return redirect('/tipos')

#TODO: Platillos
def listadoPlatillos(request):
    platillosDbb = Platillo.objects.all()
    tiposDbb = Tipo.objects.all()
    return render(request, 'platillos.html', {'platillos': list(enumerate(platillosDbb, start=1)), 'tipos': tiposDbb})

def crearPlatillo(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    precio = request.POST['precio_xg']
    disponible = request.POST['disponible_xg']
    imagen = request.FILES.get('imagen_xg')
    tipo = request.POST['tipo_xg']
    tipoSeleccionado = Tipo.objects.get(id_xg=tipo)

    if disponible == 'true':
        disponible = True

    if disponible == 'false':
        disponible = False

    newPlatillo = Platillo.objects.create(nombre_xg=nombre, descripcion_xg=descripcion, precio_xg=precio, disponible_xg=disponible, imagen_xg=imagen, tipo_xg=tipoSeleccionado)

    messages.success(request, 'Platillo creado correctamente')
    return redirect('/platillos')

def eliminarPlatillo(request, id):
    platilloDb = Platillo.objects.get(id_xg=id)
    platilloDb.delete()

    messages.success(request, 'Platillo eliminado correctamente')
    return redirect('/platillos')

def editarPlatilloView(request, id):
    platilloDb = Platillo.objects.get(id_xg=id)
    tiposDbb = Tipo.objects.all()
    return render(request, 'editarPlatillo.html', {'platillo': platilloDb, 'tipos': tiposDbb})

def actualizarPlatillo(request):
    id = request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    precio = request.POST['precio_xg']
    disponible = request.POST['disponible_xg']
    imagen = request.FILES.get('imagen_xg')
    tipo = request.POST['tipo_xg']
    tipoSeleccionado = Tipo.objects.get(id_xg=tipo)

    if disponible == 'true':
        disponible = True

    if disponible == 'false':
        disponible = False

    platilloEditar = Platillo.objects.get(id_xg=id)
    platilloEditar.nombre_xg = nombre
    platilloEditar.descripcion_xg = descripcion
    platilloEditar.precio_xg = precio
    platilloEditar.disponible_xg = disponible
    platilloEditar.tipo_xg = tipoSeleccionado
    if imagen is not None:
        platilloEditar.imagen_xg = imagen
    platilloEditar.save()

    messages.success(request, 'Platillo actualizado correctamente')
    return redirect('/platillos')

#TODO: Detalles
def listadoDetalles(request):
    detallesDbb = Detalle.objects.all()
    platillosDbb = Platillo.objects.all()
    pedidosDbb = Pedido.objects.all()
    return render(request, 'detalles.html', {
        'detalles': list(enumerate(detallesDbb, start=1)),
        'platillos': platillosDbb,
        'pedidos': pedidosDbb
    })

def crearDetalle(request):
    cantidad = request.POST['cantidad_xg']
    subtotal = request.POST['subtotal_xg']
    comentarios = request.POST['comentarios_xg']
    platillo = request.POST['platillo_xg']
    pedido = request.POST['pedido_xg']
    platilloSeleccionado = Platillo.objects.get(id_xg=platillo)
    pedidoSeleccionado = Pedido.objects.get(id_xg=pedido)

    newDetalle = Detalle.objects.create(cantidad_xg=cantidad, subtotal_xg=subtotal, comentarios_xg=comentarios, platillo_xg=platilloSeleccionado, pedido_xg=pedidoSeleccionado)

    messages.success(request, 'Detalle creado correctamente')
    return redirect('/detalles')

def eliminarDetalle(request, id):
    detalleDb = Detalle.objects.get(id_xg=id)
    detalleDb.delete()

    messages.success(request, 'Detalle eliminado correctamente')
    return redirect('/detalles')

def editarDetalleView(request, id):
    detalleDb = Detalle.objects.get(id_xg=id)
    platillosDbb = Platillo.objects.all()
    pedidosDbb = Pedido.objects.all()
    return render(request, 'editarDetalle.html', {'detalle': detalleDb, 'platillos': platillosDbb, 'pedidos': pedidosDbb})

def actualizarDetalle(request):
    id = request.POST['id_xg']
    cantidad = request.POST['cantidad_xg']
    subtotal = request.POST['subtotal_xg']
    comentarios = request.POST['comentarios_xg']
    platillo = request.POST['platillo_xg']
    pedido = request.POST['pedido_xg']
    platilloSeleccionado = Platillo.objects.get(id_xg=platillo)
    pedidoSeleccionado = Pedido.objects.get(id_xg=pedido)

    detalleEditar = Detalle.objects.get(id_xg=id)
    detalleEditar.cantidad_xg = cantidad
    detalleEditar.subtotal_xg = subtotal
    detalleEditar.comentarios_xg = comentarios
    detalleEditar.platillo_xg = platilloSeleccionado
    detalleEditar.pedido_xg = pedidoSeleccionado
    detalleEditar.save()

    messages.success(request, 'Detalle actualizado correctamente')
    return redirect('/detalles')

#TODO: Ingredientes
def listadoIngredientes(request):
    ingredientesDbb = Ingrediente.objects.all()
    return render(request, 'ingredientes.html', {
        'ingredientes': list(enumerate(ingredientesDbb, start=1))
    })

def crearIngrediente(request):
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    cantidad = request.POST['cantidad_xg']
    unidad = request.POST['unidad_xg']
    proveedor = request.POST['proveedor_xg']
    precio = request.POST['precio_xg']
    imagen = request.FILES.get('imagen_xg')

    newIngrediente = Ingrediente.objects.create(nombre_xg=nombre, descripcion_xg=descripcion, cantidad_xg=cantidad, unidad_xg=unidad, proveedor_xg=proveedor, precio_xg=precio, imagen_xg=imagen)

    messages.success(request, 'Ingrediente creado correctamente')
    return redirect('/ingredientes')

def eliminarIngrediente(request, id):
    ingredienteDb = Ingrediente.objects.get(id_xg=id)
    ingredienteDb.delete()

    messages.success(request, 'Ingrediente eliminado correctamente')
    return redirect('/ingredientes')

def editarIngredienteView(request, id):
    ingredienteDb = Ingrediente.objects.get(id_xg=id)
    return render(request, 'editarIngrediente.html', {'ingrediente': ingredienteDb})

def actualizarIngrediente(request):
    id = request.POST['id_xg']
    nombre = request.POST['nombre_xg']
    descripcion = request.POST['descripcion_xg']
    cantidad = request.POST['cantidad_xg']
    unidad = request.POST['unidad_xg']
    proveedor = request.POST['proveedor_xg']
    precio = request.POST['precio_xg']
    imagen = request.FILES.get('imagen_xg')

    ingredienteEditar = Ingrediente.objects.get(id_xg=id)
    ingredienteEditar.nombre_xg = nombre
    ingredienteEditar.descripcion_xg = descripcion
    ingredienteEditar.cantidad_xg = cantidad
    ingredienteEditar.unidad_xg = unidad
    ingredienteEditar.proveedor_xg = proveedor
    ingredienteEditar.precio_xg = precio
    if imagen is not None:
        ingredienteEditar.imagen_xg = imagen
    ingredienteEditar.save()

    messages.success(request, 'Ingrediente actualizado correctamente')
    return redirect('/ingredientes')

#TODO: Recetas
def listadoRecetas(request):
    recetasDbb = Receta.objects.all()
    platillosDbb = Platillo.objects.all()
    ingredientesDbb = Ingrediente.objects.all()
    return render(request, 'recetas.html', {
        'recetas': list(enumerate(recetasDbb, start=1)),
        'platillos': platillosDbb,
        'ingredientes': ingredientesDbb
    })

def crearReceta(request):
    cantidad = request.POST['cantidad_xg']
    observaciones = request.POST['observaciones_xg']
    instrucciones = request.POST['instrucciones_xg']
    platillo = request.POST['platillo_xg']
    ingrediente = request.POST['ingrediente_xg']
    platilloSeleccionado = Platillo.objects.get(id_xg=platillo)
    ingredienteSeleccionado = Ingrediente.objects.get(id_xg=ingrediente)

    newReceta = Receta.objects.create(cantidad_xg=cantidad, observaciones_xg=observaciones, instrucciones_xg=instrucciones, platillo_xg=platilloSeleccionado, ingrediente_xg=ingredienteSeleccionado)

    messages.success(request, 'Receta creada correctamente')
    return redirect('/recetas')

def eliminarReceta(request, id):
    recetaDb = Receta.objects.get(id_xg=id)
    recetaDb.delete()

    messages.success(request, 'Receta eliminada correctamente')
    return redirect('/recetas')

def editarRecetaView(request, id):
    recetaDb = Receta.objects.get(id_xg=id)
    platillosDbb = Platillo.objects.all()
    ingredientesDbb = Ingrediente.objects.all()

    return render(request, 'editarReceta.html', {'receta': recetaDb, 'platillos': platillosDbb, 'ingredientes': ingredientesDbb})

def actualizarReceta(request):
    id = request.POST['id_xg']
    cantidad = request.POST['cantidad_xg']
    observaciones = request.POST['observaciones_xg']
    instrucciones = request.POST['instrucciones_xg']
    platillo = request.POST['platillo_xg']
    ingrediente = request.POST['ingrediente_xg']
    platilloSeleccionado = Platillo.objects.get(id_xg=platillo)
    ingredienteSeleccionado = Ingrediente.objects.get(id_xg=ingrediente)

    recetaEditar = Receta.objects.get(id_xg=id)
    recetaEditar.cantidad_xg = cantidad
    recetaEditar.observaciones_xg = observaciones
    recetaEditar.instrucciones_xg = instrucciones
    recetaEditar.platillo_xg = platilloSeleccionado
    recetaEditar.ingrediente_xg = ingredienteSeleccionado
    recetaEditar.save()

    messages.success(request, 'Receta actualizada correctamente')
    return redirect('/recetas')

#TODO: Enviar correo
def enviarMensajeView(request):
    return render(request, 'enviarMensaje.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST['destinatario']
        asunto = request.POST['asunto']
        cuerpo = request.POST['cuerpo']

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)
        messages.success(request, 'Correo enviado correctamente')
        return redirect('/enviar-mensaje')
    return redirect('/enviar-mensaje')