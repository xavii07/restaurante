from django.contrib import admin
from .models import Provincia, Cliente, Detalle, Ingrediente, Pedido, Platillo, Tipo, Receta

# Register your models here.
admin.site.register(Provincia)
admin.site.register(Cliente)
admin.site.register(Detalle)
admin.site.register(Ingrediente)
admin.site.register(Pedido)
admin.site.register(Platillo)
admin.site.register(Tipo)
admin.site.register(Receta)
