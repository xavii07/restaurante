from django.urls import path
from . import views

urlpatterns = [
    path('', views.home),
    #TODO: Provincias
    path('provincias/', views.listadoProvincias),
    path('crearProvincia/', views.crearProvincia),
    path('eliminarProvincia/<id>', views.eliminarProvincia),
    path('editarProvincia/<id>', views.editarProvinciaView),
    path('actualizarProvincia/', views.actualizarProvincia),

    #TODO: Clientes
    path('clientes/', views.listadoClientes),
    path('crearCliente/', views.crearCliente),
    path('eliminarCliente/<id>', views.eliminarCliente),
    path('editarCliente/<id>', views.editarClienteView),
    path('actualizarCliente/', views.actualizarCliente),

    #TODO: Tipos
    path('tipos/', views.listadoTipos),
    path('crearTipo/', views.crearTipo),
    path('eliminarTipo/<id>', views.eliminarTipo),
    path('editarTipo/<id>', views.editarTipoView),
    path('actualizarTipo/', views.actualizarTipo),

    #TODO: Platillos
    path('platillos/', views.listadoPlatillos),
    path('crearPlatillo/', views.crearPlatillo),
    path('eliminarPlatillo/<id>', views.eliminarPlatillo),
    path('editarPlatillo/<id>', views.editarPlatilloView),
    path('actualizarPlatillo/', views.actualizarPlatillo),

    #TODO: Pedidos
    path('pedidos/', views.listadoPedidos),
    path('crearPedido/', views.crearPedido),
    path('eliminarPedido/<id>', views.eliminarPedido),
    path('editarPedido/<id>', views.editarPedidoView),
    path('actualizarPedido/', views.actualizarPedido),

    #TODO: Detalles
    path('detalles/', views.listadoDetalles),
    path('crearDetalle/', views.crearDetalle),
    path('eliminarDetalle/<id>', views.eliminarDetalle),
    path('editarDetalle/<id>', views.editarDetalleView),
    path('actualizarDetalle/', views.actualizarDetalle),

    #TODO: Ingredientes
    path('ingredientes/', views.listadoIngredientes),
    path('crearIngrediente/', views.crearIngrediente),
    path('eliminarIngrediente/<id>', views.eliminarIngrediente),
    path('editarIngrediente/<id>', views.editarIngredienteView),
    path('actualizarIngrediente/', views.actualizarIngrediente),

    #TODO: Recetas
    path('recetas/', views.listadoRecetas),
    path('crearReceta/', views.crearReceta),
    path('eliminarReceta/<id>', views.eliminarReceta),
    path('editarReceta/<id>', views.editarRecetaView),
    path('actualizarReceta/', views.actualizarReceta),

    #Todo: Enviar Mensaje
    path('enviar-mensaje/', views.enviarMensajeView),
    path('procesarEnvioMensaje/', views.enviar_correo),
]