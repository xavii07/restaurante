from django.db import models

# Create your models here.
class Provincia(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    capital_xg = models.CharField(max_length=50)
    region_xg = models.CharField(max_length=50)
    alcalde_xg = models.CharField(max_length=50)
    bandera_xg = models.FileField(upload_to='banderas', null=True, blank=True)
    sxgc_fecha_fundacion = models.DateField(null=True, blank=True)
    sxgc_carta_fundacion = models.FileField(upload_to='cartas', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class Cliente(models.Model):
    id_xg = models.AutoField(primary_key=True)
    cedula_xg = models.CharField(max_length=10)
    nombres_xg = models.CharField(max_length=50)
    apellidos_xg = models.CharField(max_length=50)
    direccion_xg = models.CharField(max_length=50)
    telefono_xg = models.CharField(max_length=50)
    provincia_xg = models.ForeignKey(Provincia, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nombres_xg + ' ' + self.apellidos_xg

class Pedido(models.Model):
    id_xg = models.AutoField(primary_key=True)
    fecha_xg = models.DateField()
    estado_xg = models.CharField(max_length=50)
    total_xg = models.FloatField()
    observaciones_xg = models.CharField(max_length=250)
    cliente_xg = models.ForeignKey(Cliente, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.fecha_xg) + ' ' + self.observaciones_xg

class Tipo(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=250)
    prioridad_xg = models.IntegerField()
    color_xg = models.CharField(max_length=50, null=True, blank=True)
    imagen_xg = models.FileField(upload_to='tipos', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class Platillo(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=250)
    precio_xg = models.FloatField()
    disponible_xg = models.BooleanField(default=False)
    imagen_xg = models.FileField(upload_to='platillos', null=True, blank=True)
    tipo_xg = models.ForeignKey(Tipo, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class Detalle(models.Model):
    id_xg = models.AutoField(primary_key=True)
    cantidad_xg = models.IntegerField()
    subtotal_xg = models.FloatField()
    comentarios_xg = models.CharField(max_length=250)
    platillo_xg = models.ForeignKey(Platillo, on_delete=models.CASCADE, null=True, blank=True)
    pedido_xg = models.ForeignKey(Pedido, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.comentarios_xg

class Ingrediente(models.Model):
    id_xg = models.AutoField(primary_key=True)
    nombre_xg = models.CharField(max_length=50)
    descripcion_xg = models.CharField(max_length=250)
    cantidad_xg = models.IntegerField()
    unidad_xg = models.CharField(max_length=50)
    proveedor_xg = models.CharField(max_length=50)
    precio_xg = models.FloatField()
    imagen_xg = models.FileField(upload_to='ingredientes', null=True, blank=True)

    def __str__(self):
        return self.nombre_xg

class Receta(models.Model):
    id_xg = models.AutoField(primary_key=True)
    cantidad_xg = models.IntegerField()
    observaciones_xg = models.CharField(max_length=250)
    instrucciones_xg = models.CharField(max_length=250)
    platillo_xg = models.ForeignKey(Platillo, on_delete=models.CASCADE, null=True, blank=True)
    ingrediente_xg = models.ForeignKey(Ingrediente, on_delete=models.CASCADE, null=True, blank=True)

    def __self__(self):
        return self.observaciones_xg